function countLetter(letter, sentence) {
    let result = 0;

    if(letter.length == 1) {
        for(let x = 0; x < sentence.length; x++) {
            if(sentence[x] == letter) {
                result = result + 1
            }
            else {
                result
            }
        }

        return result
    }
    else {
        return undefined
    }

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    return !text.match(/([a-z]).*\1/i) 
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
   
}

function purchase(age, price) {
    let disc = price * .20
    let discounted = price - disc

    if(age < 13) {
        return undefined
    }
    else if((age > 12 && age < 22) || age > 64) {
        return (Math.round(discounted * 100)/100).toString()
    }
    else {
        return (Math.round(price * 100)/100).toString()
    }

    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {
    let stock = items.filter(stock => stock.stocks == 0)
    let newArray = []

    for(let x = 0; x < stock.length; x++) {
        if(newArray.length == 0) {
            newArray.push(stock[x].category)
        }
        else {
            let array = newArray.filter(y => y.includes(stock[x].category))

            if(array.length > 0) {
                newArray
            }
            else {
                newArray.push(stock[x].category)
            }
        }
    }

    return newArray
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    return (candidateB.filter(y => candidateA.includes(y))).sort()
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};